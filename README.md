# nirvana-eslint

An opinionated set of rules for [ESLint]. There are many like it, but this one is mine.

[ESLint]: https://eslint.org/

## Usage

- Install the package:

  ```sh
  pnpm add -D gitlab:whatisaphone/nirvana-eslint#v0.0.0
  ```

- Create `eslint.config.js`:

  ```js
  // https://eslint.org/

  import * as nirvana from 'nirvana-eslint';

  export default [
    ...nirvana.base,
    ...nirvana.typeScript,
    // etc
  ];
  ```

- Add scripts to `package.json`:

  ```json
  {
    "scripts": {
      "lint": "pnpm lint:js .",
      "lint:js": "eslint --max-warnings 0"
    }
  }
  ```

- Add a hook to `.pre-commit-config.yaml`:

  ```yaml
  - id: lint:js
    name: lint:js
    files: \.[jt]sx?$
    language: system
    require_serial: true
    entry: pnpm lint:js --fix
  ```

- Add a step in CI:

  ```yaml
  script:
    - pnpm lint
  ```

## Plugins

Legend:

- ✅ Included in `base` config
- ➕ Included in another config
- ❓ Under consideration
- ❌ Rejected

Plugins:

- ❌ [babel] – Pretty much covered by the TypeScript rules, who cares about plain JS anymore.
- ❓ [canonical]
- ❓ [compat]
- ➕ [css-modules]
- ✅ [eslint-comments]
- ❌ [filenames] – Who cares
- ✅ [functional]
- ❌ [@getify/proper-arrows] – All these rules are either useless, or already covered by other plugins.
- ❓ [html]
- ✅ [import]
- ❌ [import-helpers] – This is basically a customizable version of [simple-import-sort].
- ❌ [jest]
- ✅ [jsdoc]
- ➕ [jsx-a11y]
- ❌ [misc] – Many minimally useful rules, works poorly with TypeScript
- ❌ [@mysticatea] – Some interesting rules, but bundled with a bunch of extra stuff I don't need. See also [this issue](https://github.com/mysticatea/eslint-plugin/issues/23).
- ❓ [n]
- ❌ [node] – Unmaintained, forked into [n]
- ❓ [no-only-tests]
- ❌ [ocd] – Only does import sorting, which [simple-import-sort] already does.
- ❌ [optimize-regex] – Blocked on [upstream bugs](https://github.com/DmitrySoshnikov/regexp-tree/issues/162) in [regexp-tree].
- ✅ [prettier]
- ✅ [promise]
- ➕ [react]
- ➕ [react-hooks]
- ➕ [react-refresh]
- ✅ [regexp]
- ❓ [security]
- ❌ [shopify] – This has some useful rules, but mixes in a bunch of pinned dependencies and is a pain to use without version conflicts.
- ✅ [simple-import-sort]
- ➕ [solid]
- ✅ [sonarjs]
- ❌ [total-functions] – Interesting ideas, but it crashed ESLint when I tried it
- ❌ [tree-shaking] – Seems abandoned. I got a bunch of errors like `Unknown node type SpreadElement.`
- ❓ [tsdoc]
- ➕ [typescript-eslint]
- ✅ [unicorn]
- ✅ [unused-imports]
- ➕ [vitest]
- ❓ [yml]

[babel]: https://github.com/babel/eslint-plugin-babel
[canonical]: https://github.com/gajus/eslint-plugin-canonical
[compat]: https://github.com/amilajack/eslint-plugin-compat
[css-modules]: https://github.com/atfzl/eslint-plugin-css-modules
[eslint-comments]: https://github.com/eslint-community/eslint-plugin-eslint-comments
[filenames]: https://github.com/selaux/eslint-plugin-filenames
[functional]: https://github.com/eslint-functional/eslint-plugin-functional
[@getify/proper-arrows]: https://github.com/getify/eslint-plugin-proper-arrows
[html]: https://github.com/yeonjuan/html-eslint
[import]: https://github.com/import-js/eslint-plugin-import
[import-helpers]: https://github.com/Tibfib/eslint-plugin-import-helpers
[jest]: https://github.com/jest-community/eslint-plugin-jest
[jsdoc]: https://github.com/gajus/eslint-plugin-jsdoc
[jsx-a11y]: https://github.com/jsx-eslint/eslint-plugin-jsx-a11y
[misc]: https://github.com/iliubinskii/eslint-plugin-misc
[@mysticatea]: https://github.com/mysticatea/eslint-plugin
[n]: https://github.com/eslint-community/eslint-plugin-n
[node]: https://github.com/mysticatea/eslint-plugin-node
[no-only-tests]: https://github.com/levibuzolic/eslint-plugin-no-only-tests
[ocd]: https://github.com/ciena-blueplanet/eslint-plugin-ocd
[optimize-regex]: https://github.com/BrainMaestro/eslint-plugin-optimize-regex
[prettier]: https://github.com/prettier/eslint-plugin-prettier
[promise]: https://github.com/eslint-community/eslint-plugin-promise
[react]: https://github.com/jsx-eslint/eslint-plugin-react
[react-hooks]: https://github.com/facebook/react/tree/master/packages/eslint-plugin-react-hooks
[react-refresh]: https://github.com/ArnaudBarre/eslint-plugin-react-refresh
[regexp]: https://github.com/ota-meshi/eslint-plugin-regexp
[regexp-tree]: https://github.com/DmitrySoshnikov/regexp-tree
[security]: https://github.com/eslint-community/eslint-plugin-security
[shopify]: https://github.com/Shopify/eslint-plugin-shopify
[simple-import-sort]: https://github.com/lydell/eslint-plugin-simple-import-sort
[solid]: https://github.com/solidjs-community/eslint-plugin-solid
[sonarjs]: https://github.com/SonarSource/eslint-plugin-sonarjs
[total-functions]: https://github.com/danielnixon/eslint-plugin-total-functions
[tree-shaking]: https://github.com/lukastaegert/eslint-plugin-tree-shaking
[tsdoc]: https://github.com/microsoft/tsdoc
[typescript-eslint]: https://typescript-eslint.io/
[unicorn]: https://github.com/sindresorhus/eslint-plugin-unicorn
[unused-imports]: https://github.com/sweepline/eslint-plugin-unused-imports
[vitest]: https://github.com/vitest-dev/eslint-plugin-vitest
[yml]: https://github.com/ota-meshi/eslint-plugin-yml

## Cutting a release

- Push to master

- Wait for CI to pass

- Edit version in `package.json`

- Run:

```sh
git add package.json
version=$(<package.json jq -r .version)
git commit -m"v${version}"
git push
git tag "v${version}"
git push origin "v${version}"
```
