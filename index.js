import eslintJs from '@eslint/js';
import pluginEslintComments from '@eslint-community/eslint-plugin-eslint-comments/configs';
import pluginVitest from '@vitest/eslint-plugin';
import confusingBrowserGlobals from 'confusing-browser-globals';
import configPrettier from 'eslint-config-prettier';
import pluginCssModules from 'eslint-plugin-css-modules';
import pluginFunctional from 'eslint-plugin-functional';
import pluginImport from 'eslint-plugin-import';
import pluginJsdoc from 'eslint-plugin-jsdoc';
import pluginJsxA11y from 'eslint-plugin-jsx-a11y';
import pluginPromise from 'eslint-plugin-promise';
import pluginReact from 'eslint-plugin-react';
import pluginReactHooks from 'eslint-plugin-react-hooks';
import pluginReactRefresh from 'eslint-plugin-react-refresh';
import pluginRegexp from 'eslint-plugin-regexp';
import pluginSimpleImportSort from 'eslint-plugin-simple-import-sort';
import pluginSolid from 'eslint-plugin-solid';
import pluginSonarjs from 'eslint-plugin-sonarjs';
import pluginUnicorn from 'eslint-plugin-unicorn';
import pluginUnusedImports from 'eslint-plugin-unused-imports';
import globals from 'globals';
import typescriptEslint from 'typescript-eslint';

export const base = [
  /*
   * ESLint
   * https://eslint.org/
   */
  eslintJs.configs.recommended,
  {
    rules: {
      eqeqeq: 'error',
      'no-console': 'error',
      'no-restricted-globals': ['error', ...confusingBrowserGlobals],
      'no-useless-computed-key': 'error',
      'no-useless-rename': 'error',
      'object-shorthand': 'error',
    },
  },
  {
    // Include all *.js not in a subdirectory. This includes things like
    // eslint/stylelint/babel/webpack configs, etc.
    files: ['*.js'],
    languageOptions: {
      globals: globals.node,
    },
  },

  /*
   * eslint-plugin-eslint-comments
   * https://github.com/eslint-community/eslint-plugin-eslint-comments
   */
  pluginEslintComments.recommended,
  {
    rules: {
      '@eslint-community/eslint-comments/disable-enable-pair': [
        'error',
        { allowWholeFile: true },
      ],
    },
  },

  /*
   * eslint-plugin-functional
   * https://github.com/eslint-functional/eslint-plugin-functional
   */
  {
    plugins: {
      functional: pluginFunctional,
    },
    rules: {
      // Currying

      'functional/functional-parameters': 'off',

      // No Exceptions

      'functional/no-promise-reject': 'error',
      'functional/no-throw-statements': 'error',
      'functional/no-try-statements': 'off',

      // No Mutations

      'functional/immutable-data': 'off',
      'functional/no-let': ['error', { allowInFunctions: true }],
      'functional/prefer-immutable-types': 'off', // requires type checking
      'functional/prefer-readonly-type': 'off',
      'functional/type-declaration-immutability': [
        'error',
        {
          rules: [{ identifiers: '', immutability: 'ReadonlyShallow' }],
        },
      ],

      // No Other Paradigms

      'functional/no-class-inheritance': 'off',
      'functional/no-classes': 'off',
      'functional/no-mixed-types': 'off',
      'functional/no-this-expressions': 'off',

      // No Statements

      'functional/no-conditional-statements': 'off',
      'functional/no-expression-statements': 'off',
      'functional/no-loop-statements': 'off',
      'functional/no-return-void': 'off',

      // Stylistic

      'functional/prefer-property-signatures': 'error',
      'functional/prefer-tacit': 'error',
      'functional/readonly-type': ['error', 'keyword'],
    },
  },

  /*
   * eslint-plugin-import
   * https://github.com/import-js/eslint-plugin-import
   */
  {
    ...pluginImport.flatConfigs.recommended,
    languageOptions: {}, // why does it try to set this?
  },
  {
    rules: {
      'import/first': 'error',
      'import/namespace': 'off', // Too slow in large projects
      'import/newline-after-import': 'error',
      'import/no-cycle': ['error', { ignoreExternal: true }],
      'import/no-named-default': 'error',
      'import/no-unresolved': 'off',
      // https://github.com/import-js/eslint-plugin-import/issues/3079
      // 'import/no-unused-modules': ['error', { unusedExports: true }],
    },
  },

  /*
   * eslint-plugin-jsdoc
   * https://github.com/gajus/eslint-plugin-jsdoc
   */
  pluginJsdoc.configs['flat/recommended'],
  {
    rules: {
      'jsdoc/require-jsdoc': 'off',
    },
  },

  /*
   * eslint-plugin-promise
   * https://github.com/eslint-community/eslint-plugin-promise
   */
  pluginPromise.configs['flat/recommended'],
  {
    rules: {
      'promise/always-return': 'off',
    },
  },

  /*
   * eslint-plugin-regexp
   * https://github.com/ota-meshi/eslint-plugin-regexp
   */
  pluginRegexp.configs['flat/recommended'],

  /*
   * eslint-plugin-simple-import-sort
   * https://github.com/lydell/eslint-plugin-simple-import-sort
   */
  {
    plugins: {
      'simple-import-sort': pluginSimpleImportSort,
    },
    rules: {
      'simple-import-sort/exports': 'error',
      'simple-import-sort/imports': 'error',
    },
  },

  /*
   * eslint-plugin-sonarjs
   * https://github.com/SonarSource/SonarJS/blob/master/packages/jsts/src/rules/README.md
   */
  pluginSonarjs.configs.recommended,
  {
    rules: {
      'sonarjs/assertions-in-tests': 'off', // covered by vitest/expect-expect
      'sonarjs/no-duplicate-string': 'off',
    },
  },

  /*
   * eslint-plugin-unicorn
   * https://github.com/sindresorhus/eslint-plugin-unicorn
   */
  pluginUnicorn.configs['flat/recommended'],
  {
    rules: {
      'unicorn/consistent-function-scoping': 'off',
      // https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1993
      'unicorn/custom-error-definition': 'off',
      'unicorn/explicit-length-check': ['error', { 'non-zero': 'not-equal' }],
      'unicorn/no-array-callback-reference': 'off',
      'unicorn/no-array-reduce': 'off',
      'unicorn/no-null': 'off',
      'unicorn/no-unused-properties': 'error',
      'unicorn/no-useless-undefined': 'off',
      'unicorn/prefer-module': 'off',
      'unicorn/prefer-query-selector': 'off',
      'unicorn/prefer-switch': 'off',
      'unicorn/prevent-abbreviations': 'off',
      'unicorn/switch-case-braces': ['error', 'avoid'],
      // https://github.com/sindresorhus/eslint-plugin-unicorn/issues/1690#issuecomment-1033685083
      'unicorn/text-encoding-identifier-case': 'off',
    },
  },

  /*
   * eslint-plugin-unused-imports
   * https://github.com/sweepline/eslint-plugin-unused-imports
   */
  {
    plugins: {
      'unused-imports': pluginUnusedImports,
    },
    rules: {
      'no-unused-vars': 'off',
      'unused-imports/no-unused-imports': 'error',
      'unused-imports/no-unused-vars': [
        'error',
        {
          vars: 'all',
          varsIgnorePattern: '^_',
          args: 'all',
          argsIgnorePattern: '^_',
          caughtErrors: 'all',
        },
      ],
    },
  },

  /*
   * eslint-config-prettier
   * https://github.com/prettier/eslint-config-prettier
   */
  configPrettier,
];

export const vite = [
  {
    rules: {
      'import/no-useless-path-segments': [
        'error',
        { noUselessIndex: true, commonjs: true },
      ],
    },
  },
];

export const typeScript = [
  /*
   * typescript-eslint
   * https://typescript-eslint.io/
   */
  ...typescriptEslint.configs.strictTypeChecked.map((conf) => ({
    files: ['**/*.{ts,tsx}'],
    ...conf,
  })),
  ...typescriptEslint.configs.stylisticTypeChecked.map((conf) => ({
    files: ['**/*.{ts,tsx}'],
    ...conf,
  })),
  {
    files: ['**/*.{ts,tsx}'],
    languageOptions: {
      parserOptions: {
        projectService: true,
      },
    },
  },
  {
    files: ['**/*.{ts,tsx}'],
    rules: {
      '@typescript-eslint/explicit-member-accessibility': 'error',
      '@typescript-eslint/explicit-module-boundary-types': [
        'error',
        {
          // We already have @typescript-eslint/no-explicit-any
          allowArgumentsExplicitlyTypedAsAny: true,
        },
      ],
      '@typescript-eslint/naming-convention': [
        'error',
        { selector: 'default', format: ['strictCamelCase'] },
        { selector: 'typeLike', format: ['StrictPascalCase'] },
        {
          selector: 'import',
          format: ['strictCamelCase', 'StrictPascalCase'],
        },
        // allow PascalCase for react components
        {
          selector: 'function',
          format: ['strictCamelCase', 'StrictPascalCase'],
        },
        {
          selector: 'parameter',
          modifiers: ['unused'],
          format: ['strictCamelCase'],
          leadingUnderscore: 'allow',
        },
        // don't lint plain object keys, e.g. a request headers dict
        { selector: 'objectLiteralProperty', format: null },
      ],
      '@typescript-eslint/no-confusing-void-expression': [
        'error',
        { ignoreArrowShorthand: true },
      ],
      '@typescript-eslint/no-restricted-types': [
        'error',
        {
          types: {
            // https://github.com/microsoft/TypeScript/issues/30825
            Omit: {
              message:
                "Prefer a strongly-typed alternative such as type-fest's `Except`.",
              fixWith: 'Except',
            },
          },
        },
      ],
      '@typescript-eslint/no-unused-vars': 'off', // Handled by eslint-plugin-unused-imports
      '@typescript-eslint/restrict-template-expressions': [
        'error',
        {
          allowNumber: true,
        },
      ],
      '@typescript-eslint/strict-boolean-expressions': 'error',
      '@typescript-eslint/switch-exhaustiveness-check': 'error',
    },
  },

  /*
   * eslint-plugin-functional
   * https://github.com/eslint-functional/eslint-plugin-functional
   */
  {
    files: ['**/*.{ts,tsx}'],
    rules: {
      'functional/prefer-immutable-types': [
        'error',
        {
          parameters: {
            enforcement: 'ReadonlyShallow',
            ignoreInferredTypes: true,
            ignoreTypePattern: 'Event(?:$|<)',
          },
          returnTypes: {
            enforcement: 'None',
          },
          variables: {
            enforcement: 'ReadonlyShallow',
            ignoreInFunctions: true,
          },
        },
      ],
    },
  },

  /*
   * eslint-plugin-import
   * https://github.com/import-js/eslint-plugin-import
   */
  {
    files: ['**/*.{ts,tsx}'],
    ...pluginImport.flatConfigs.typescript,
  },

  /*
   * eslint-plugin-jsdoc
   * https://github.com/gajus/eslint-plugin-jsdoc
   */
  {
    files: ['**/*.{ts,tsx}'],
    ...pluginJsdoc.configs['flat/recommended-typescript'],
  },
  {
    files: ['**/*.{ts,tsx}'],
    rules: {
      'jsdoc/require-jsdoc': 'off',
    },
  },
];

const jsx = [
  /*
   * eslint-plugin-jsx-a11y
   * https://github.com/jsx-eslint/eslint-plugin-jsx-a11y
   */
  pluginJsxA11y.flatConfigs.strict,

  /*
   * eslint-plugin-react
   * https://github.com/jsx-eslint/eslint-plugin-react
   */
  pluginReact.configs.flat.recommended,
  pluginReact.configs.flat['jsx-runtime'],
  {
    rules: {
      'react/button-has-type': 'error',
      'react/display-name': 'off',
      'react/jsx-boolean-value': ['error', 'always'],
      'react/jsx-child-element-spacing': 'error',
      'react/jsx-curly-brace-presence': 'error',
      'react/jsx-fragments': 'error',
      'react/jsx-no-useless-fragment': 'error',
      'react/no-invalid-html-attribute': 'error',
      'react/no-unused-prop-types': 'error',
      'react/self-closing-comp': 'error',
    },
  },
];

export const react = [
  ...jsx,

  /*
   * eslint-plugin-react
   * https://github.com/jsx-eslint/eslint-plugin-react
   */
  {
    rules: {
      'react/destructuring-assignment': 'error',
      'react/forbid-dom-props': [
        'error',
        {
          forbid: [
            {
              propName: 'onInput',
              // This is quoted from https://react.dev/reference/react-dom/components/input
              message: 'It is idiomatic to use onChange instead',
            },
            {
              propName: 'onInputCapture',
              message: 'It is idiomatic to use onChangeCapture instead',
            },
          ],
        },
      ],
      'react/jsx-no-constructed-context-values': 'error',
      'react/no-array-index-key': 'error',
      'react/no-unstable-nested-components': 'error',
    },
  },

  /*
   * eslint-plugin-react-hooks
   * https://github.com/facebook/react/tree/main/packages/eslint-plugin-react-hooks
   */
  {
    plugins: {
      'react-hooks': pluginReactHooks,
    },
    rules: pluginReactHooks.configs.recommended.rules,
  },

  /*
   * eslint-plugin-react-refresh
   * https://github.com/ArnaudBarre/eslint-plugin-react-refresh
   */
  pluginReactRefresh.configs.recommended,
];

export const solid = [
  ...jsx,

  /*
   * eslint-plugin-jsx-a11y
   * https://github.com/jsx-eslint/eslint-plugin-jsx-a11y
   */
  {
    rules: {
      // https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/issues/894
      'jsx-a11y/label-has-associated-control': 'off',
    },
  },

  /*
   * eslint-plugin-react
   * https://github.com/jsx-eslint/eslint-plugin-react
   */
  {
    rules: {
      'react/no-unknown-property': ['error', { ignore: ['class', 'for'] }],
    },
  },

  /*
   * Solid ESLint Plugin
   * https://github.com/solidjs-community/eslint-plugin-solid
   */
  pluginSolid.configs.recommended,
];

export const cssModules = [
  /*
   * eslint-plugin-css-modules
   * https://github.com/atfzl/eslint-plugin-css-modules
   */
  {
    plugins: {
      'css-modules': pluginCssModules,
    },
    ...pluginCssModules.configs.recommended,
  },
];

export const vitest = [
  /*
   * @vitest/eslint-plugineslint-plugin-vitest
   * https://github.com/vitest-dev/eslint-plugin-vitest
   */
  {
    files: ['**/*.test.*'],
    ...pluginVitest.configs.all,
  },
  {
    files: ['**/*.test.*'],
    rules: {
      'vitest/padding-around-all': 'off',
      'vitest/padding-around-expect-groups': 'off',
      'vitest/prefer-expect-assertions': 'off',
      'vitest/require-top-level-describe': 'off',
    },
  },
];
